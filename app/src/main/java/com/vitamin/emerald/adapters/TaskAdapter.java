package com.vitamin.emerald.adapters;

import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.vitamin.emerald.R;
import com.vitamin.emerald.activities.ProgressBarActivity;
import com.vitamin.emerald.activities.TasksActivity;
import com.vitamin.emerald.dataobjects.Training;
import com.vitamin.emerald.helpers.Helper;

import java.util.List;

/**
 * Created by Vitamin on 02.12.2017.
 */

public class TaskAdapter extends BaseAdapter {

    private List<Training> trainingList;
    private TasksActivity activity;

    public TaskAdapter(List<Training> trainingList, TasksActivity activity) {
        this.trainingList = trainingList;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        if (trainingList!=null) {
            return trainingList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i) {
        if (trainingList!=null) {
            return trainingList.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        view = activity.getLayoutInflater().inflate(R.layout.one_task, null);
        TextView tvName = view.findViewById(R.id.tvTaskMainContent);
        tvName.setText(trainingList.get(i).getName());
        ImageButton buttonStart = view.findViewById(R.id.ibStartTask);
        buttonStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Helper.training = trainingList.get(i);
                activity.startActivity(new Intent(activity, ProgressBarActivity.class));
            }
        });
        return view;
    }
}
