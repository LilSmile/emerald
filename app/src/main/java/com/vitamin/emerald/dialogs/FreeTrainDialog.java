package com.vitamin.emerald.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.vitamin.emerald.R;
import com.vitamin.emerald.activities.ProgressBarActivity;
import com.vitamin.emerald.dataobjects.Training;
import com.vitamin.emerald.helpers.Helper;
import com.vitamin.emerald.modes.Mode;
import com.vitamin.emerald.notifications.Type;

import static com.vitamin.emerald.helpers.Constants.MODE;

/**
 * Created by Vitamin on 02.12.2017.
 */

public class FreeTrainDialog extends DialogFragment {
    private Type type;

    public FreeTrainDialog() {

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.trainingSettings);
        View view = getActivity().getLayoutInflater().inflate(R.layout.challenge_setting_dialog, null);
        final EditText editText = view.findViewById(R.id.etCountItems);
        final Button voiceButton = view.findViewById(R.id.buttonVoice);
        final Button textButton = view.findViewById(R.id.buttonText);

        voiceButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                voiceButton.setTextColor(getResources().getColor(R.color.orange));
                textButton.setTextColor(getResources().getColor(R.color.grey));
                type = Type.SOUND;
            }
        });

        textButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textButton.setTextColor(getResources().getColor(R.color.orange));
                voiceButton.setTextColor(getResources().getColor(R.color.grey));
                type = Type.HEADS_UP;
            }
        });

        builder.setPositiveButton(R.string.start, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                String stringCOunt = editText.getEditableText().toString();
                int count = Integer.valueOf(stringCOunt).intValue();
                Training training = new Training();
                training.setproducts_amount(count);
                training.setTrain_type("free");
                if (Type.HEADS_UP.equals(type)) {
                    training.setNotification_type("text");
                } else {
                    training.setNotification_type("robovoice");
                }
                Intent intent = new Intent(getActivity(), ProgressBarActivity.class);
                intent.putExtra(MODE, 0);
                Helper.training = training;
                startActivity(intent);
                dismiss();
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });

        return null;
    }
}
