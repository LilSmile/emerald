package com.vitamin.emerald.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


import com.vitamin.emerald.R;
import com.vitamin.emerald.dialogs.FreeTrainDialog;
import com.vitamin.emerald.helpers.Helper;

public class MainActivity extends AppCompatActivity {

    Button tasksButton, freeButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tasksButton = findViewById(R.id.tasksButton);
        freeButton = findViewById(R.id.freeButton);

        Helper.setContext(this);
        Helper.buildApi();

        tasksButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(MainActivity.this, TasksActivity.class));
                    }
                });
            }
        });

        freeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FreeTrainDialog trainDialog = new FreeTrainDialog();
                trainDialog.show(getFragmentManager(), "trainDialog");
            }
        });
    }

}
