package com.vitamin.emerald.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.vitamin.emerald.R;
import com.vitamin.emerald.adapters.TaskAdapter;
import com.vitamin.emerald.dataobjects.Training;
import com.vitamin.emerald.helpers.Helper;

import java.util.List;

public class TasksActivity extends AppCompatActivity {

    ListView listViewTasks;
    TaskAdapter adapter;
    List<Training> trainingList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tasks);

        listViewTasks = findViewById(R.id.listViewTasks);
        Helper.setContext(this);
        Helper.buildApi();
        Helper.getTasks(this);

    }

    public void setListTasks(List<Training> trainingList) {
        this.trainingList = trainingList;
        adapter = new TaskAdapter(trainingList, this);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                listViewTasks.setAdapter(adapter);
                listViewTasks.invalidate();
            }
        });

    }
}
