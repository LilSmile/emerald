package com.vitamin.emerald.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.vitamin.emerald.R;
import com.vitamin.emerald.dataobjects.Training;
import com.vitamin.emerald.helpers.Constants;
import com.vitamin.emerald.helpers.Helper;

public class ProgressBarActivity extends AppCompatActivity implements Constants{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_bar);
        int mode = getIntent().getIntExtra(MODE, -1);
        switch (mode) {
            case -1: {
                Log.wtf("smth", "went wrong");
                break;
            }
            case 0: {
                Helper.createTraining(this, Helper.training);
                break;
            }
            case 1: {
                Helper.getTask(this, Helper.training.getId());
                break;
            }
        }
    }

    public void startTraining(Training training) {
        
    }
}
