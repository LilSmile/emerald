package com.vitamin.emerald.modes;

/**
 * Created by Vitamin on 02.12.2017.
 */

public enum Mode {
    TIME, RESULT, ACCURACY, LENGTH;
}
