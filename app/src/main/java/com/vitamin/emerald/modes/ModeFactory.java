package com.vitamin.emerald.modes;

/**
 * Created by Vitamin on 02.12.2017.
 */

public class ModeFactory {

    public static AbstractMode getMode(Mode mode) {
        AbstractMode abstractMode;
        switch (mode) {
            case TIME: {
                abstractMode = new TimeMode();
                break;
            }
            case LENGTH: {
                abstractMode = new LengthMode();
                break;
            }
            case RESULT: {
                abstractMode = new ResultMode();
                break;
            }
            case ACCURACY: {
                abstractMode = new AccuracyMode();
                break;
            }
            default: {
                abstractMode = new TimeMode();
                break;
            }
        }
        return abstractMode;
    }

}
