package com.vitamin.emerald.notifications;

import android.app.Activity;

import com.vitamin.emerald.modes.Mode;

/**
 * Created by Vitamin on 02.12.2017.
 */

public class NotificationFactory {

    public static AbstractNotification getNotification(Type type) {
        AbstractNotification notification;
        switch (type) {
            case LONG: {
                notification = new LongToastNotification();
                break;
            }
            case SHORT: {
                notification = new ShortToastNotification();
                break;
            }
            case SOUND: {
                notification = new SoundNotification();
                break;
            }
            case HEADS_UP: {
                notification = new HeadsUpNotification();
                break;
            }
            default: {
                notification = new SoundNotification();
                break;
            }
        }
        return notification;
    }

}
