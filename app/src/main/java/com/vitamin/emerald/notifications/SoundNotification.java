package com.vitamin.emerald.notifications;

/**
 * Created by Vitamin on 02.12.2017.
 */

import android.speech.tts.TextToSpeech;

import java.util.Locale;


public class SoundNotification extends AbstractNotification {

    private TextToSpeech tts;

    public SoundNotification() {
        super();
    }

    @Override
    public void showNotification() {
        tts = new TextToSpeech(activity, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    tts.setLanguage(Locale.UK);
                }
            }
        });
        tts.speak(content, TextToSpeech.QUEUE_FLUSH, null);
        tts.stop();
        tts.shutdown();
    }
}
