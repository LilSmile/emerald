package com.vitamin.emerald.notifications;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

import com.vitamin.emerald.helpers.Constants;

/**
 * Created by Vitamin on 02.12.2017.
 */

public class HeadsUpNotification extends AbstractNotification implements Constants {

    public HeadsUpNotification() {
        super();
    }

    @Override
    public void showNotification() {
        NotificationManager manager = (NotificationManager) activity.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(activity);
        builder.setContentTitle(content);
        manager.notify(NOTIFICATION_ID, builder.build());

    }
}
