package com.vitamin.emerald.notifications;

import android.app.Activity;
import android.content.Context;

/**
 * Created by Vitamin on 02.12.2017.
 */

public abstract class AbstractNotification {

    protected long id;
    protected String content;
    protected Activity activity;

    public void setData(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public AbstractNotification() {
    }

    public abstract void showNotification();

}
