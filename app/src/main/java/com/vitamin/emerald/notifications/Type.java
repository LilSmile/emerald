package com.vitamin.emerald.notifications;

/**
 * Created by Vitamin on 02.12.2017.
 */

public enum Type {
    SOUND, HEADS_UP, LONG, SHORT;
}
