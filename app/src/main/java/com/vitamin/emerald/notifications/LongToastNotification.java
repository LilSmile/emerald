package com.vitamin.emerald.notifications;

import android.widget.Toast;

/**
 * Created by Vitamin on 02.12.2017.
 */

public class LongToastNotification extends AbstractNotification {

    public LongToastNotification() {
        super();
    }

    @Override
    public void showNotification() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(activity, content, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
