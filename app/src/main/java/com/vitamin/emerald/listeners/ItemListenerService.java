package com.vitamin.emerald.listeners;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import ru.evotor.framework.core.action.event.receipt.position_edited.PositionAddedEvent;
import ru.evotor.framework.receipt.Position;


/**
 * Created by Vitamin on 02.12.2017.
 */

public class ItemListenerService extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        PositionAddedEvent event = PositionAddedEvent.create(intent.getExtras());
        Position position = event.getPosition();
        String uuid = position.getUuid();
        Log.wtf("got item", uuid);
    }
}
