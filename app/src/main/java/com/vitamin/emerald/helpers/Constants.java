package com.vitamin.emerald.helpers;

/**
 * Created by Vitamin on 02.12.2017.
 */

public interface Constants {

    String APP_PREFERENCES = "EMERALD_APP";
    String CURRENT_UUID = "current_uuid";
    int NOTIFICATION_ID = 420;
    String API_LINK = "https://evotor.enhancelab.ru/_api/";
    String MODE = "mode"; //0 - create, 1 - get

}
