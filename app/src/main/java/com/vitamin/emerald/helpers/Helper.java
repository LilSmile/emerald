package com.vitamin.emerald.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vitamin.emerald.activities.ProgressBarActivity;
import com.vitamin.emerald.activities.TasksActivity;
import com.vitamin.emerald.dataobjects.Training;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.evotor.framework.users.User;
import ru.evotor.framework.users.UserApi;

/**
 * Created by Vitamin on 02.12.2017.
 */

public class Helper implements Constants {

    private static Context context;
    private static SharedPreferences preferences;
    private static Retrofit retrofit;
    private static EvatorAPI api;
    public static Training training;


    public static void buildApi() {
        if (retrofit == null) {
            Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ssZ")
                    .create();
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(API_LINK)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build();
            api = retrofit.create(EvatorAPI.class);
        }
    }

    public static void getTasks(TasksActivity tasksActivity) {
        List<Training> trainings = new ArrayList<>();
        Training training = new Training();

        /*
        User user = UserApi.getAuthenticatedUser(tasksActivity);
        Call<List<Training>> call = api.getTrainings(user.getUuid());
        try {
            Response<List<Training>> response = call.execute();
            List<Training> listTrainings = response.body();
            tasksActivity.setListTasks(listTrainings);
        } catch (IOException e) {

        }*/

    }

    public static void getTask(ProgressBarActivity activity, int taskId) {
        Call<Training> call = api.getTraining(taskId);
        try {
            Response<Training> response = call.execute();
            Training training = response.body();
            activity.startTraining(training);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void setContext(Context newContext) {
        context = newContext;
        if (preferences == null) {
            preferences = context.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);
        }
    }

    public static boolean isRightItem(String uuid) {
        String savedUUID = preferences.getString(CURRENT_UUID, "");
        return savedUUID.equals(uuid);
    }

    public static void saveCurrentItem(String uuid) {
        preferences.edit().putString(CURRENT_UUID, uuid).commit();
    }

    public static void createTraining(ProgressBarActivity activity, Training training) {
        Call<Training> call = api.createTraining(training);
        try {
            Response<Training> response = call.execute();
            Training training1 = response.body();
            activity.startTraining(training1);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


}
