package com.vitamin.emerald.helpers;

import com.vitamin.emerald.dataobjects.Training;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by Vitamin on 02.12.2017.
 */

public interface EvatorAPI {

    @GET("tasks/{user_id}/")
    Call<List<Training>> getTrainings(@Path("user_id") String userId);

    @GET("tasks/{task_id}/")
    Call<Training> getTraining(@Path("task_id") int task_id);

    @FormUrlEncoded
    @POST("tasks/create/")
    Call<Training> createTraining(@Body Training training);

    @FormUrlEncoded
    @POST("tasks/pass/")
    Call<Training> passTraining(@Body Training training);
}
