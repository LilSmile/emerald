package com.vitamin.emerald.dataobjects;

/**
 * Created by Vitamin on 02.12.2017.
 */

public class Result {

    private String product_id;
    private int fails;
    private long time_spent;

    public Result() {
    }

    public Result(String product_id, int fails, long time_spent) {

        this.product_id = product_id;
        this.fails = fails;
        this.time_spent = time_spent;
    }

    public String getProduct_id() {

        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public int getFails() {
        return fails;
    }

    public void setFails(int fails) {
        this.fails = fails;
    }

    public long getTime_spent() {
        return time_spent;
    }

    public void setTime_spent(long time_spent) {
        this.time_spent = time_spent;
    }
}
