package com.vitamin.emerald.dataobjects;

import java.util.List;

/**
 * Created by Vitamin on 02.12.2017.
 */

public class Training {

    private int id;
    private String user_id;
    private String name;
    private int products_amount;

    public Training() {
    }

    public Training(int id, String user_id, String name, int products_amount, String train_type, String notification_type, String status, List<Product> products_list, long timestamp_created, long timestamp_finished, long due_date, List<Result> results_list) {

        this.id = id;
        this.user_id = user_id;
        this.name = name;
        this.products_amount = products_amount;
        this.train_type = train_type;
        this.notification_type = notification_type;
        this.status = status;
        this.products_list = products_list;
        this.timestamp_created = timestamp_created;
        this.timestamp_finished = timestamp_finished;
        this.due_date = due_date;
        this.results_list = results_list;
    }

    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTrain_type() {
        return train_type;
    }

    public void setTrain_type(String train_type) {
        this.train_type = train_type;
    }

    public String getNotification_type() {
        return notification_type;
    }

    public void setNotification_type(String notification_type) {
        this.notification_type = notification_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Product> getProducts_list() {
        return products_list;
    }

    public void setProducts_list(List<Product> products_list) {
        this.products_list = products_list;
    }

    public long getTimestamp_created() {
        return timestamp_created;
    }

    public void setTimestamp_created(long timestamp_created) {
        this.timestamp_created = timestamp_created;
    }

    public long getTimestamp_finished() {
        return timestamp_finished;
    }

    public void setTimestamp_finished(long timestamp_finished) {
        this.timestamp_finished = timestamp_finished;
    }

    public long getDue_date() {
        return due_date;
    }

    public void setDue_date(long due_date) {
        this.due_date = due_date;
    }

    public List<Result> getResults_list() {
        return results_list;
    }

    public void setResults_list(List<Result> results_list) {
        this.results_list = results_list;
    }

    String train_type; //'free'|'challenge'
    String notification_type; // 'audio'|'robovoice'|'text'
    String status; // 'finished'|'failed'|'open'|'active'
    List<Product> products_list;
    long timestamp_created;
    long timestamp_finished;
    long due_date;
    List<Result> results_list;

    public int getproducts_amount() {
        return products_amount;
    }

    public void setproducts_amount(int products_amount) {
        this.products_amount = products_amount;
    }
}
